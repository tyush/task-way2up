@extends('layouts.auth')

@section('auth_title') Регистрация @endsection

@section('form')
    <form method="post" action="{{ route('user_signup') }}">
        @csrf
        <div class="card card-login card-hidden">
            <div class="card-header text-center">
                <h4 class="card-title">Регистрация</h4>
            </div>
            <div class="card-content">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">face</i>
                    </span>
                    <div class="form-group label-floating">
                        <label class="control-label">Имя</label>
                        <input name="name" type="text" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">face</i>
                    </span>
                    <div class="form-group label-floating">
                        <label class="control-label">Эл. почта</label>
                        <input name="email" type="text" class="form-control" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">lock_outline</i>
                    </span>
                    <div class="form-group label-floating">
                        <label class="control-label">Пароль</label>
                        <input name="password" type="password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-wd btn-lg btn-success">Готово</button>
            </div>
        </div>
    </form>
@endsection
