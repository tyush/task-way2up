@extends('layouts.auth')

@section('auth_title') Логин @endsection

@section('form')
    <form method="post" action="{{ route('user_signin') }}">
        @csrf
        <div class="card card-login card-hidden">
            <div class="card-header text-center">
                <h4 class="card-title">Логин</h4>
            </div>
            <div class="card-content">
                @if(session()->has('invalid_user'))
                    <h5 class="c-red text-center">Неправильный email или пароль</h5>
                @endif
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">face</i>
                    </span>
                    <div class="form-group label-floating">
                        <label class="control-label">Эл. почта</label>
                        <input name="email" type="text" class="form-control" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">lock_outline</i>
                    </span>
                    <div class="form-group label-floating">
                        <label class="control-label">Пароль</label>
                        <input name="password" type="password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-wd btn-lg btn-success">Войти</button>
            </div>
        </div>
    </form>
@endsection
