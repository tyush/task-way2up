<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="http://www.urbanui.com/" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>@yield('auth_title')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="{{ asset('dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/assets/css/turbo.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/assets/css/demo.css') }}" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
</head>
<body>

<a href="{{ route('home') }}">
    <i class="fa fa-home back_to_home"></i>
</a>
<nav class="navbar navbar-primary navbar-transparent navbar-absolute">
    <div class="container">
        <div class="navbar-header">
        </div>
    </div>
</nav>
<div class="wrapper wrapper-full-page">
    <div class="full-page login-page"  data-color="blue">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        @yield('form')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script src="{{ asset('dashboard/assets/vendors/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/material.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.validate.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/moment.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/chartist.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.bootstrap-wizard.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap-notify.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery-jvectormap.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/nouislider.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.select-bootstrap.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.datatables.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/sweetalert2.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/fullcalendar.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.tagsinput.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/turbo.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/demo.js') }}"></script>

<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();
        setTimeout(function() {
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>
</html>
