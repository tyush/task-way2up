<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Dashboard</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link rel="apple-touch-icon" sizes="76x76" href="http://www.urbanui.com/" />
    <link href="{{ asset('dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/assets/css/turbo.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/assets/css/fileinput.css') }}">
    <link href="{{ asset('dashboard/assets/css/demo.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/editor/codemirror.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/editor/froala_editor.pkgd.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/editor/froala_style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/editor/codemirror5.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/multiselect/select.css') }}" rel="stylesheet"/>
    <link href="{{ asset('dashboard/css/main.css') }}" rel="stylesheet">
    @yield('style')
</head>

<body>
<div class="wrapper">
    <div class="sidebar">
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li>
                    <a href="{{ route('admin.dashboard') }}" class="@if($route == 'admin/dashboard') active_page @endif">
                        <i class="material-icons">dashboard</i>
                        <p>Панель управления</p>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.products.index') }}" class="@if($route == 'admin/products') active_page @endif">
                        <i class="fa fa-list-alt "></i>
                        <p>Товары </p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-absolute" data-topbar-color="blue">
            <div class="container-fluid">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                        <i class="material-icons visible-on-sidebar-regular f-26">keyboard_arrow_left</i>
                        <i class="material-icons visible-on-sidebar-mini f-26">keyboard_arrow_right</i>
                    </button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse languages">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="" class="logout">
                                <i class="fa fa-sign-out"></i>
                            </a>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script src="{{ asset('dashboard/assets/vendors/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/material.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.validate.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/moment.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/chartist.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.bootstrap-wizard.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap-notify.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery-jvectormap.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/nouislider.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.select-bootstrap.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.datatables.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/sweetalert2.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/fullcalendar.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/vendors/jquery.tagsinput.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/turbo.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/fileinput.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/demo.js') }}"></script>
<script src="{{ asset('dashboard/editor/codemirror.min.js') }}"></script>
<script src="{{ asset('dashboard/editor/codemirror5.js') }}"></script>
<script src="{{ asset('dashboard/editor/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('dashboard/js/main.js') }}"></script>
<script>
    $(function () {
        @if(session()->has('added'))
            swal({
                type:'success',
                title:'Добавлен!',
                showConfirmButton:false,
                timer:2000
            }).catch(swal.loop);
        @endif
        @if(session()->has('updated'))
            swal({
                type:'success',
                title:'Обновлен!',
                showConfirmButton:false,
                timer:2000
            }).catch(swal.loop);
        @endif
        @if(session()->has('deleted'))
            swal({
                type:'success',
                title:'Удалён!',
                showConfirmButton:false,
                timer:2000
            }).catch(swal.loop);
        @endif
    });
</script>
@yield('script')
</html>
