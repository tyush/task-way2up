<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-expand-sm bg-light">
                <ul class="navbar-nav d-flex justify-content-between" style="width:100%">
                    <div>
                        <li class="nav-item">
                            <a class="nav-link" href="/">Главная</a>
                        </li>
                    </div>
                    @auth
                        <div class="d-flex">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Добро пожаловать {{ Auth::user()->name }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('user_logout') }}">Выйти</a>
                            </li>
                        </div>
                    @else
                        <div class="d-flex">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('user_login') }}">Логин</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('user_register') }}">Регистрация</a>
                            </li>
                        </div>
                    @endauth
                </ul>
            </nav>
            @yield('content')
        </div>
    </div>
</div>

<script src="/main/js/jquery.min.js"></script>
<script src="/main/js/bootstrap.min.js"></script>
<script src="/main/js/main.js"></script>

</body>
</html>
