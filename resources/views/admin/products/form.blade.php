<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Заглавие', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Краткое описание', ['class' => 'control-label']) !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('href') ? 'has-error' : ''}}">
    {!! Form::label('href', 'Путь', ['class' => 'control-label']) !!}
    {!! Form::text('href', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Цена', ['class' => 'control-label']) !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
    {!! Form::label('content', 'Полное описание', ['class' => 'control-label']) !!}
    {!! Form::textarea('content', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
</div>

<div class="form-group text-right">
    {!! Form::submit($formMode === 'edit' ? 'Обновить' : 'Добавить', ['class' => 'btn btn-success']) !!}
</div>