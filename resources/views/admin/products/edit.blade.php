@extends('layouts.admin')

@section('content')
    <div class="card-header">
        <a href="{{ url('/admin/products') }}">
            <button class="btn btn-warning btn-sm">
                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
            </button>
        </a>
    </div>
    <div class="card-body edit_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

       {!! Form::model($product, [
           'method' => 'PATCH',
           'url' => ['/admin/products', $product->id],
           'class' => 'form-horizontal',
           'files' => true
       ]) !!}

       @include ('admin.products.form', ['formMode' => 'edit'])

       {!! Form::close() !!}
    </div>
@endsection