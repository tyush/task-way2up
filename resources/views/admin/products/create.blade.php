@extends('layouts.admin')

@section('content')
    <div class="card-header">
        <a href="{{ url('/admin/products') }}">
            <button class="btn btn-warning btn-sm">
                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
            </button>
        </a>
    </div>
    <div class="card-body create_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['url' => '/admin/products', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include ('admin.products.form', ['formMode' => 'create'])

        {!! Form::close() !!}
    </div>
@endsection