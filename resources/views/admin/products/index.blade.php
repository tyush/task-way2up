@extends('layouts.admin')

@section('content')
    <div class="card-header title_search">
        <div>
            <a href="{{ url ('admin/products/create') }}" class="btn btn-success btn-sm">
                <i class="fa fa-plus" aria-hidden="true"></i> Добавить
            </a>
        </div>
        <div>
            {!! Form::open(['method' => 'GET', 'url' => '/admin/products', 'class' => 'form-inline', 'role' => 'search'])  !!}
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Поиск..." value="{{ request('search') }}">
                <span class="input-group-append">
                    <button class="btn btn-warning" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                     @if(request()->has('search'))
                        <a class="btn btn-warning" href="{{ url('/admin/products') }}">
                            <i class="fa fa-remove"></i>
                        </a>
                    @endif
                </span>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="card-body index_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Заглавие</th>
                        <th>Путь</th>
                        <th style="text-align: right">Действия</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($products as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->href }}</td>
                        <td style="text-align: right">
                            <a href="{{ url('/admin/products/' . $item->id) }}" title="View Product"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Посмотреть</button></a>
                            <a href="{{ url('/admin/products/' . $item->id . '/edit') }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редактировать</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/admin/products', $item->id],
                                'style' => 'display:inline',
                                'class' => 'delete_form'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Удалять', array(
                                'type' => 'button',
                                'class' => 'btn btn-danger btn-sm delete',
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $products->appends(['search' => Request::get('search')])->render() !!} </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click','.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: 'Вы уверены ?',
                    text: 'Вернуть обратно невозможно.',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Да удалить',
                    cancelButtonText: 'Отменить',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: 'Отменен!',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            })
        })
    </script>
@endsection