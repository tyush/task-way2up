@extends('layouts.admin')

@section('content')
    <div class="card-header">
        <a href="{{ url('/admin/products') }}">
            <button class="btn btn-warning btn-sm">
                <i class="fa fa-arrow-left" aria-hidden="true"></i> Назад
            </button>
        </a>
        <a href="{{ url('/admin/products/' . $product->id . '/edit') }}" title="Edit Product">
            <button class="btn btn-primary btn-sm">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редактировать
            </button>
        </a>

        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['/admin/products', $product->id],
            'style' => 'display:inline',
            'class' => 'delete_form'
        ]) !!}
        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Удалять', array(
            'type' => 'button',
            'class' => 'btn btn-danger btn-sm delete',
        )) !!}
        {!! Form::close() !!}
    </div>

    <div class="card-body show_body">
        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{ $product->id }}</td>
                    </tr>
                    <tr>
                        <th> Заглавие</th>
                        <td> {{ $product->title }} </td>
                    </tr>
                    <tr>
                        <th> Краткое описание</th>
                        <td> {{ $product->description }} </td>
                    </tr>
                    <tr>
                        <th> Полное описание</th>
                        <td> {{ $product->content }} </td>
                    </tr>
                    <tr>
                        <th> Путь</th>
                        <td> {{ $product->href }} </td>
                    </tr>
                    <tr>
                        <th> Цена</th>
                        <td> {{ $product->price }} </td>
                    </tr>
                    <tr>
                        <th> Дата публикации</th>
                        <td> {{ $product->created_at }} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('body').on('click', '.delete', function () {
                let form = $(this).parents('form.delete_form');
                swal({
                    title: 'Вы уверены ?',
                    text: 'Вернуть обратно невозможно.',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Да удалить',
                    cancelButtonText: 'Отменить',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger m-l-10',
                    buttonsStyling: false,
                }).then(() => {
                    form.submit();
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        swal({
                            title: 'Отменен!',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        }).catch(swal.noop);
                    }
                })
            })
        })
    </script>
@endsection