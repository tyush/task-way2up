@extends('layouts.admin')

@section('title') Панель управления @endsection

@section('content')
    <div class="card-body create_body">
        @if ($errors->any())
            <ul class="alert alert-danger list_type_none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['url' => '/admin/update', 'class' => 'form-horizontal']) !!}
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Эл. почта ', ['class' => 'control-label']) !!}
                {!! Form::text('email', Auth::user()->email, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group {{ $errors->has('new_password') ? 'has-error' : ''}}">
                {!! Form::label('new_password', 'Новый пароль', ['class' => 'control-label']) !!}
                <input id="new_password" type="password" name="new_password" class="form-control new_password">
            </div>
            <div class="form-group old_password @if(session()->get('wrong',null)===true) old_password_wrong has-error @endif">
                {!! Form::label('current_password', 'Текущий пароль', ['class' => 'control-label']) !!}
                <input id="current_password" type="password" name="current_password" class="form-control">
            </div>
            <div class="form-group text-right">
                {!! Form::submit('Обновить', ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $(".new_password").keyup(function () {
                let current = $(this).val();
                if (current){
                    $('.old_password').slideDown();
                } else{
                    $('.old_password').slideUp();
                }
            })
        })
    </script>
@endsection