@extends('layouts.main')

@section('title') {{ $product->title }} @endsection

@section('content')
    <div class="col-md-12 text-center">
        <div class="post">
            <a class="post-img"><img src="/main/img/post-1.jpg" alt=""></a>
            <div class="post-body">
                <div class="post-meta">
                    <span class="post-date">{{\Carbon\Carbon::parse($product->created_at)->formatLocalized('%d %B %Y')}}</span>
                </div>
                <h3 class="post-title">
                    <p>{{ $product->title }}</p>
                    <p>{{ $product->description }}</p>
                    <p>{{ $product->content }}</p>
                    <p>{{ $product->price }}</p>
                </h3>
            </div>
        </div>
    </div>
@endsection
