@extends('layouts.main')

@section('title') Главная @endsection

@section('content')
    <div class="row">
        @foreach($products as $product)
            <div class="col-md-4">
                <div class="post">
                    <a class="post-img"><img src="/main/img/post-1.jpg" style="width: 100%" alt=""></a>
                    <div class="post-body">
                        <div class="post-meta">
                            <span class="post-date">{{\Carbon\Carbon::parse($product->created_at)->formatLocalized('%d %B %Y')}}</span>
                        </div>
                        <h3 class="post-title">
                            <p>{{ $product->title }}</p>
                        </h3>
                        <p>{{ $product->description }}</p>
                        <a href="{{ route('show',$product->href) }}" style="float:right;color: #00B0FF">Перейти
                            <i class="fa fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="col-md-12">
        {!! $products->appends(request()->all())->render() !!}
    </div>
@endsection