<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::insert(
            [
                [
                    'title' => 'Product 1 title',
                    'description' => 'Product 1 description',
                    'content' => 'Product 1 content',
                    'href' => 'product-1-href',
                    'price' => 1000,
                    'created_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'title' => 'Product 2 title',
                    'description' => 'Product 2 description',
                    'content' => 'Product 2 content',
                    'href' => 'product-2-href',
                    'price' => 1000,
                    'created_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'title' => 'Product 3 title',
                    'description' => 'Product 3 description',
                    'content' => 'Product 3 content',
                    'href' => 'product-3-href',
                    'price' => 1000,
                    'created_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'title' => 'Product 4 title',
                    'description' => 'Product 4 description',
                    'content' => 'Product 4 content',
                    'href' => 'product-4-href',
                    'price' => 1000,
                    'created_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'title' => 'Product 5 title',
                    'description' => 'Product 5 description',
                    'content' => 'Product 5 content',
                    'href' => 'product-5-href',
                    'price' => 1000,
                    'created_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'title' => 'Product 6 title',
                    'description' => 'Product 6 description',
                    'content' => 'Product 6 content',
                    'href' => 'product-6-href',
                    'price' => 1000,
                    'created_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'title' => 'Product 7 title',
                    'description' => 'Product 7 description',
                    'content' => 'Product 7 content',
                    'href' => 'product-7-href',
                    'price' => 1000,
                    'created_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'title' => 'Product 8 title',
                    'description' => 'Product 8 description',
                    'content' => 'Product 8 content',
                    'href' => 'product-8-href',
                    'price' => 1000,
                    'created_at' => date('Y-m-d H:i:s'),
                ],
            ]
        );
    }
}
