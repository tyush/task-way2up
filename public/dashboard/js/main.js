$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //Logout
    $('body').on('click','.nav a.logout', function(e){
        e.preventDefault();
        swal({
            text: 'Вы действительно хотите выйти ?',
            type: 'success',
            showCancelButton: true,
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        }).then(() => {
            window.location.href = '/admin/logout';
        }, (dismiss) => {}).catch(swal.loop);
    });

    //Floara
    $('.floara-textarea').froalaEditor({
        imageUploadURL: '/admin/froala-image-upload',
        imageUploadMethod: 'POST',
        imageUploadParam: 'image',
        requestHeaders: {
            'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
        }
    }).on('froalaEditor.image.uploaded', function (e, editor, response) {
        console.log(e,editor,response);
    });

    //Multi select
    $('.selectpicker').selectpicker();
});
