<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Main\IndexController@index')->name('home');
Route::get('/{href}', 'Main\IndexController@show')->name('show');

Route::prefix('/user')->group(function (){
    Route::get('login','User\AuthController@login')->name('user_login');
    Route::get('register','User\AuthController@register')->name('user_register');
    Route::post('signin','User\AuthController@signin')->name('user_signin');
    Route::post('signup','User\AuthController@signup')->name('user_signup');
    Route::get('logout','User\AuthController@logout')->name('user_logout');
});

Route::prefix('/admin')->name('admin.')->group(function (){
    Route::get('login','Admin\AuthController@login')->name('login');
    Route::post('signin','Admin\AuthController@signin')->name('signin');
    Route::middleware('admin_middle')->group(function (){
        Route::get('dashboard', 'Admin\AdminController@index')->name('dashboard');
        Route::get('logout','Admin\AuthController@logout')->name('logout');
        Route::post('update','Admin\AdminController@update')->name('user_update');
        Route::resource('products', 'Admin\ProductsController');
    });
});

Auth::routes();