<?php

namespace App\Http\Controllers\Main;
use App\Http\Controllers\Controller;
use App\Models\Product;

class IndexController extends Controller{
    public function index(){
        $products = Product::latest()->paginate(3);
        return view('main.index',compact('products'));
    }

    public function show($href){
        $product = Product::where('href',$href)->firstOrFail();
        return view('main.show',compact('product'));
    }
}
