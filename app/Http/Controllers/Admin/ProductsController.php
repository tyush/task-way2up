<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 3;

        if (!empty($keyword)) {
            $products = Product::where('title', 'LIKE', "%$keyword%")
                ->orWhere('href', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $products = Product::latest()->paginate($perPage);
        }

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $requestData['href'] = Str::slug($requestData['href']);
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'href' => 'required|max:255|unique:products',
            'content' => 'required',
            'price' => 'required',
        ];
        $validate = Validator::make($requestData,$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }
        Product::create($requestData);
        return redirect('admin/products')->with('added', 'Product added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('admin.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $requestData = $request->all();
        $requestData['href'] = Str::slug($requestData['href']);
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'href' => "required|max:255|unique:products,href,".$product->id,
            'content' => 'required',
            'price' => 'required',
        ];
        $validate = Validator::make($requestData,$rules);
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput($request->input());
        }
        $product->update($requestData);

        return redirect('admin/products')->with('updated', 'Product updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return redirect('admin/products')->with('deleted', 'Product deleted!');
    }
}
