<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller{

    public function login(){
        if (Auth::guest()){
            return view('admin.login');
        }
        else{
            return redirect(route('admin.dashboard'));
        }
    }

    public function signin(Request $request){
        $validator = Validator::make($request->all(),[
            'email' => 'required|email|max:255',
            'password' => 'required|min:8|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('invalid_admin',true)->withInput($request->input());
        }
        $userdata = array(
            'email'  => $request->email,
            'password'  => $request->password
        );
        if (Auth::attempt($userdata) && Auth::user()->role_id==1) {
            return redirect(route('admin.dashboard'));
        }
        return redirect()->back()->with('invalid_admin',true)->withInput($request->input());
    }


    public function logout(){
        Auth::logout();
        return redirect(route('admin.login'));
    }

}
