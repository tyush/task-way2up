<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function index(){
        return view('admin.dashboard');
    }

    public function update(Request $request){
        $requestData = $request->all();
        $wrong_password = $changed_password = false;
        $rules = [
            'email' => 'required|email',
        ];
        if ($requestData['new_password']){
            $check = Hash::check($requestData['current_password'],Auth::user()->password);
            if ($check){
                $changed_password = true;
                $rules['new_password'] = 'required|min:8';
            }
            else{
                $wrong_password = true;
            }
        }

        $validate = Validator::make($requestData,$rules);
        if ($validate->fails() || $wrong_password){
            return redirect()->back()->withErrors($validate)->with([
                'wrong' => $wrong_password
            ]);
        }

        $update = ['email' => $requestData['email']];
        if ($changed_password){
            $update['password'] = Hash::make($requestData['new_password']);
        }

        User::where('id',Auth::id())->update($update);
        return redirect()->back()->with('updated', 'User updated!');
    }
}
