<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;

class AuthController extends Controller{

    public function login(){
        if (Auth::guest()){
            return view('user.login');
        }
        else{
            return redirect(route('home'));
        }
    }

    public function register(){
        return view('user.register');
    }

    public function signin(Request $request){
        $validator = Validator::make($request->all(),[
            'email' => 'required|email|max:255',
            'password' => 'required|min:8|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('invalid_user',true)->withInput($request->input());
        }
        $data = array(
            'email'  => $request->get('email'),
            'password'  => $request->get('password')
        );
        if (Auth::attempt($data) && Auth::user()->role_id == 2) {
            return redirect(route('home'));
        }
        return redirect()->back()->with('invalid_user',true)->withInput($request->input());
    }

    public function signup(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:8|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->role_id = 2;
        $user->save();
        $data = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];
        Auth::attempt($data);
        return redirect(route('home'));
    }

    public function logout(){
        Auth::logout();
        return redirect(route('user_login'));
    }

}
