<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;

class Controller extends BaseController{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $segments;
    public $route;

    public function __construct(){
        $this->route= Route::getFacadeRoot()->current()->uri();
        $this->segments = explode('/', parse_url(url()->current())['path'] ?? '');
        $this->share();
    }

    public function share(){
        View::share([
            'route' => $this->route,
            'url_segments' => $this->segments,
        ]);
    }
}
